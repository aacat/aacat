#coding:utf-8

from bottle import route, run, template, hook, response

def get_from_jsonfile():
    """
    保存してある猫記事のJSONファイルを読み込む
    """
    with open('test.json') as f:
        return f.read()

def get_articles():
    from GuidesiteAPI import GuidesiteAPI
    api = GuidesiteAPI()
    posts = get_from_jsonfile()
    articles = api.get_article_data(posts)
    return articles

def get_image_tags():
    articles = get_articles()
    from ContentsAPI import ContentsAPI
    api = ContentsAPI()
    images = api.get_cat_images(articles)
    return images

def get_images(num):
    """
    numの枚数のimgを返す
    （ランダムに返すとかできたらうれしい）
    """
    data = get_image_tags()
    import random
    random.shuffle(data)
    data = data[0:int(num)]
    response = {
            'data': data,
            'len': len(data),
            }
    return response
#    import json
#    return json.dumps(response)

@hook('after_request')
def enable_cors():
    response.headers['Access-Control-Allow-Origin'] = '*'

@route('/images/<num>')
def index(num):
    return get_images(num)

run(host='localhost', port=8080)
