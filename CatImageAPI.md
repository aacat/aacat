# 猫画像を取得してくるAPI

## 使い方

### Endpoint

http://localhost/images


### 形式

    {
        data:
        画像情報の配列

        [
            {
                image_tag:
                <img>タグまるごと
                例:<img src="..." alt="..." width="100" height="200">

                parent_article:
                親記事のURL
                例:http://allabout.co.jp/gm/gc/448420/

                title:
                記事タイトル

                abstract:
                記事概要
            } //(画像の数回繰り返し)
        ]
        len:
        画像の数
    }


