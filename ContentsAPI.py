#coding:utf-8

from itertools import repeat

class ContentsAPI():
    def get_cat_images(self, article_data):
        """
        article_idsに含まれる画像を返す
        @param
          article_data {'id':articleID, 'abstract':概要, 'title':title}
        """
        from BeautifulSoup import BeautifulSoup
        images = []
        for article in article_data:
            id = article['id']
            title = article['title']
            abstract = article['abstract']
            content = self.get_content(id)
            soup = BeautifulSoup(content)
            # imgタグを探す
            imgs = soup.findAll('img')
            # imgタグにparentをセットにする
            article_url = 'http://allabout.co.jp/gm/gc/{0}/'.format(id)
            objs = []
            for img in imgs:
                obj = {
                        'image_tag': str(img),
                        'parent_article': article_url,
                        'article_title': title,
                        'article_abstract': abstract,
                        }
                objs.append(obj)
            images.extend(objs)
        
        # imagesをurlともとページへのリンクのペアに直す
        return images

    def get_content(self, id):
        """
        article_idから記事本文を取得して返す
        """
        import urllib2
        endpoint = 'http://api.allabout.co.jp/allabout/article/contents/{0}'
        url = endpoint.format(id)
        response = urllib2.urlopen(url)
        jsondata = response.read()
        #print(jsondata)
        import json
        jsonobj = json.loads(jsondata)
        content = jsonobj[u'data'][0][u'content']
        return content

if __name__ == '__main__':
    api = ContentsAPI()
    images = api.get_cat_images([449286, 448420])
    print(images)

