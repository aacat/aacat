# coding:utf-8

"""
APIを叩いてjsonをとってくる
"""


CAT_GSID = '1247'

class GuidesiteAPI():
    def get_json(self, gs_id, num):
        """
        Guidesite APIを叩いて新着記事情報をとってくる
        @param
        gs_id Guidesite ID
        num 取得する新着記事数
        """
        import urllib2
        gs_endpoint = 'http://api.allabout.co.jp/allabout/guidesite/latest/{0}/{1}'
        url = gs_endpoint.format(gs_id, num)
        response = urllib2.urlopen(url)
        html = response.read()
        print(html)

    def get_from_stdin(self):
        """
        標準入力から文字列をとってくる(test用)
        """
        import sys
        return sys.stdin.read()

    def parse_articles(self, jsondata):
        """
        jsondataをパースしてarticle IDだけを取り出し、
        そのリストを返す
        """
        import json
        jsonobj = json.loads(jsondata)
        articles = []
        for article in jsonobj[u'data']:
            id = article[u'article_id']
            title = article[u'article_title']
            abstract = article[u'article_abstract']
            articles.append({'id':id, 'title': title, 'abstract': abstract})
        #print(json.dumps(jsonobj, indent=4))
        return articles

    def get_article_data(self, posts=''):
        if posts == '':
            posts = self.get_from_stdin()
        arts = self.parse_articles(posts)
        return arts

if __name__ == '__main__':
    api = GuidesiteAPI()
    #api.get_json(CAT_GSID, 1000)
    posts = api.get_from_stdin()
    #post_ids = api.parse_article_id(posts)
    article_ids = api.get_article_ids(posts)
    print(article_ids)
